import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner,BarcodeScannerOptions } from '@ionic-native/barcode-scanner';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
options:BarcodeScannerOptions;
encodText:string='';
encodedData:any={};
scannerData:any={};
  constructor(public navCtrl: NavController, public scanner:BarcodeScanner) {

  }
scan(){
  this.options={
prompt:'scan your barcode'
  };
  this.scanner.scan().then((data) => {
    this.scannerData=data;
  },(err) => {
    console.log('error',err);
  })
}
encode(){
  this.scanner.encode(this.scanner.Encode.TEXT_TYPE,this.encodText).then((data) => {
    this.encodedData=data;
  },(err) => {
    console.log('error',err);
  })
}
}
